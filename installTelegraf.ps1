echo "Creating C:\Install..."
New-Item -ItemType Directory -Force -Path C:\Install
cd C:\Install
echo "Removing old telegraf_windows_amd64.zip..."
Remove-Item telegraf_windows_amd64.zip -Force
echo "Downloading https://s3.eu-central-1.amazonaws.com/pbcom-files/pbcom.cz/telegraf_windows_amd64.zip..."
wget.exe https://s3.eu-central-1.amazonaws.com/pbcom-files/pbcom.cz/telegraf_windows_amd64.zip --debug --no-check-certificate -O telegraf_windows_amd64.zip
echo "Unzipping telegraf_windows_amd64.zip to C:\Install\telegraf..."
unzip -o -j telegraf_windows_amd64.zip -d C:\Install\telegraf
echo "Stopping telegraf service..."
stop-service -name telegraf -force
echo "Creating C:\Program Files\telegraf..."
New-Item -ItemType Directory -Force -Path "C:\Program Files\telegraf"
echo "Copying telegraf\telegraf.exe to C:\Program Files\telegraf..."
copy telegraf\telegraf.exe "C:\Program Files\telegraf"
cd "C:\Program Files\telegraf"
echo "Removing telegraf.conf..."
Remove-Item telegraf.conf -Force
echo "Downloading https://gitlab.com/pbcom_bobac/telegraf_general/raw/master/conf/generic_server.conf..."
wget.exe https://gitlab.com/pbcom_bobac/telegraf_general/raw/master/conf/generic_server.conf --no-check-certificate
echo "Renaming downloaded conf to telegraf.conf..."
ren generic_server.conf telegraf.conf
if (Get-Service "telegraf" -ErrorAction SilentlyContinue)
{
  echo "Telegraf service already installed."
} else
{
  echo "Installing Telegraf service..."
  .\telegraf -service install
}
echo "Staring Telegraf"
Start-Service -name "Telegraf Data Collector Service"
echo "Done."
