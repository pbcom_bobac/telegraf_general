param(
$TelegrafUrl = 'http://monitor.pbcom.cz:8086',
$TelegrafDb = 'telegraf',
$TelegrafDbUser = '',
$TelegrafDbPassword = '',
$TelegrafCustomer = 'unknown',
$TelegrafOS = 'windows',
$TelegrafOSVersion = 'unknown'
)


[System.Environment]::SetEnvironmentVariable('TELEGRAF_URL',"$TelegrafUrl",[System.EnvironmentVariableTarget]::Machine)
[System.Environment]::SetEnvironmentVariable('TELEGRAF_DB',"$TelegrafDb",[System.EnvironmentVariableTarget]::Machine)
[System.Environment]::SetEnvironmentVariable('TELEGRAF_DBUSER',"$TelegrafDbUser",[System.EnvironmentVariableTarget]::Machine)
[System.Environment]::SetEnvironmentVariable('TELEGRAF_DBPASSWORD',"$TelegrafDbPassword",[System.EnvironmentVariableTarget]::Machine)
[System.Environment]::SetEnvironmentVariable('TELEGRAF_CUSTOMER',"$TelegrafCustomer",[System.EnvironmentVariableTarget]::Machine)
[System.Environment]::SetEnvironmentVariable('TELEGRAF_OS',"$TelegrafOS",[System.EnvironmentVariableTarget]::Machine)
[System.Environment]::SetEnvironmentVariable('TELEGRAF_OSVERSION',"$TelegrafOSVersion",[System.EnvironmentVariableTarget]::Machine)

echo "Paramaters set"

echo "TELEGRAF_URL: $TelegrafUrl"
echo "TELEGRAF_DB: $TelegrafDb"
echo "TELEGRAF_DBUSER: $TelegrafDbUser"
echo "TELEGRAF_DBPASSWORD: **********"
echo "TELEGRAF_CUSTOMER: $TelegrafCustomer"
echo "TELEGRAF_OS: $TelegrafOS"
echo "TELEGRAF_OSVERSION: $TelegrafOSVersion"

echo "Finished."
